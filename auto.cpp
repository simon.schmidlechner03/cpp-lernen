/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <iostream>
#include <string>
using namespace std;
class Auto {
    public:
    Auto(string marke, string model) :m_marke(marke), m_model(model) {}
    void info(){
        cout << " marke : " << m_marke << " model : " << m_model << endl;
    }
    private:
    string m_marke;
    string m_model;
};
int main()
{
    cout << "Hello World" << endl;
    Auto a("tesla","s");
    a.info();

    return 0;
}
